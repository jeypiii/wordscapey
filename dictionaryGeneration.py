#!/usr/bin/env python3

import json
import os
FileNotFoundError = IOError

def validateGeneratesDictionary(sourceDictionary, outputFolder):
    raise NotImplementedError

def generateDictionariesFromJSON(sourceDictionary, outputFolder=None, dictionaryFormat=None, keyFormat=None, generateFunc=None):  #make this genwric abd add a loader/generator func then generators foe each format will be a specialization of this
    outputFolder = outputFolder or "./"     #this is safer than making it default diectly as callers of this function are able to not override it with None
    if not os.access(outputFolder, 1):
        os.mkdir(outputFolder)

    try:
        dictionary = loadDictionaryJSON(sourceDictionary)
    except (FileNotFoundError, ValueError):
        print("Invalid dictionary. Dictionary generation failed.")
        return
    
    dictionary = partitionDictionary(dictionary, keyFormat=keyFormat)
    for key in dictionary:
        print("Generating dictionary for %s." % key)
        d = dict.fromkeys(dictionary[key])
        for i in d:
            d[i] = 1
        generateFunc(d, key[0], key[1:], outputFolder, dictionaryFormat)

    print("Dictionaries generated in the folder %s." % outputFolder)

def generateDictionaryLengthsJSON(sourceDictionary, outputFile, keyFormat=None):
    try:
        dictionary = loadDictionaryJSON(sourceDictionary)
    except (FileNotFoundError, ValueError):
        print("Invalid dictionary. Dictionary generation failed.")
        return
    
    dictionary = getDictionaryLengthsJSON(dictionary, keyFormat=keyFormat)
    #assumes that outputFile can be created with enough permissions
    file = open(outputFile, "w")
    json.dump(dictionary, file, separators=(',', ':'))
    file.close()
    
    print("Dictionary Lengths generated at %s" % outputFile)
    

def getDictionaryLengthsJSON(dictionary, keyFormat=None):
    output = partitionDictionary(dictionary, keyFormat=keyFormat)
    for key in output:
        output[key] = len(output[key])
    
    return output


def generateDictionaryTxt(dictionary, letter, length, outputFolder=".", dictionaryFormat=None):     #90% copied from generateDictionaryJSON; refactor that 
    if outputFolder[-1] != "/":    #replace with os.isDirectory and related funcs
        outputFolder += "/"

    dictionaryFormat = dictionaryFormat or "words_{letter}{length}"
    #for now, assumes that outputFolder exists
    #also assumes that the script is given enough permissions

    try:    #move to checkDictionaryFormat?
        file = dictionaryFormat.format(letter=letter, length=length)
    except KeyError:
        try:
            file = dictionaryFormat.format(letter, length)
        except IndexError:
            print("Dictionary format is invalid. Format must contain 2 instances of {} or one {letter} and one {length}.")
            return

    file = open(outputFolder + file, "w")
    for word in dictionary:
        file.write(word + "\n")     #only properly viewable on editors with a proper unix mode, but that is okay as this was never meant to be human-readable anyway
    file.close()

def generateDictionaryJSON(dictionary, letter, length, outputFolder=".", dictionaryFormat=None):
    if outputFolder[-1] != "/":    #replace with os.isDirectory and related funcs
        outputFolder += "/"
    
    dictionaryFormat = dictionaryFormat or "words_{letter}{length}.json"
    #for now, assumes that outputFolder exists
    #also assumes that the script is given enough permissions

    try:    #move to checkDictionaryFormat?
        file = dictionaryFormat.format(letter=letter, length=length)
    except KeyError:
        try:
            file = dictionaryFormat.format(letter, length)
        except IndexError:
            print("Dictionary format is invalid. Format must contain 2 instances of {} or one {letter} and one {length}.")
            return
        
    file = open(outputFolder + file, "w")
    json.dump(dictionary, file, separators=(',', ':'))
    file.close()

def partitionDictionary(dictionary, keyFormat=None):
    keyFormat = keyFormat or "{letter}{length}"

    output = {}
    for word in dictionary:
        letter = word[0]
        length = len(word)
        
        try:    #move to checkDictionaryFormat?
            key = keyFormat.format(letter=letter, length=length)
        except KeyError:
            try:
                key = keyFormat.format(letter, length)
            except IndexError:
                print("Key format is invalid. Format must contain 2 instances of {} or one {letter} and one {length}.")
                return
        
        if key not in output:
            output[key] = []
        
        output[key].append(word)
    
    return output

def loadDictionaryJSON(sourceDictionary):
    try:
        file = open(sourceDictionary)
    except FileNotFoundError:
        print("Source dictionary file does not exist")
        file.close()
        raise

    try:
        dictionary = json.load(file)
    except ValueError:
        print("Source dictionary supplied is not valid JSON.")
        raise
    finally:
        file.close()

    return dictionary


if __name__ == "__main__":
    import os
    os.chdir("/mnt/sdcard/qpython/projects3/Wordscapey")
    print(os.getcwd())
    dictionary = input("Input name of dictionary: ")
    generateDictionariesFromJSON(dictionary, "dictionaries", generateFunc=generateDictionaryTxt)
    generateDictionaryLengthsJSON(dictionary, "dictionaryLengths")
