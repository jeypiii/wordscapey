#!/usr/bin/env python3

from itertools import permutations
from collections import Counter
from string import ascii_lowercase as alphabet
import json
import os
import threading

FileNotFoundError = IOError    #In python 3.3, IOError was turned into a mere alias to OSError and a more
                               #specific error FileNotFoundError (a subclass of OSError) is now used when
                               #a file can't be found. Putting it here so that porting to python 3.3 would be easier
                               #see PEP 3151 for a more in-depth look

def listToStr(List, end=""):
    outputStr = ""
    for item in List:
        outputStr += item + end

    return outputStr

def getIntPos(string, start=0, end=None, reverse=False, signed=False):
    len_string = len(string)
    if end == None:
        end = len_string

    start, end = map(lambda i: len_string + i if i < 0 else i, (start, end))
    if not reverse:
        for i in range(start, end):
            if signed and i == start and string[i] in ("-", "+"):
                continue

            if not string[i].isnumeric():
                return (start, i)

    else:
        for i in range(end-1, start-1, -1):
            if signed and i != end-1 and string[i] in ("-", "+"):
                return (i, end)
            
            if not string[i].isnumeric():
                return (i+1, end)

    return (start, end)

def makeThreadSafe(func):
    #For making a funcion only active in one thread
    #Primarily used to protect calls to cacheDictionary() so that checks for
    #  existing dictionaries do not suffer from a race event
    def o(*args, **kwargs):
        o.lock.acquire()
        try:
            value = func(*args, **kwargs)
        except:
            raise
        finally:
            o.lock.release()

        return value
    o.lock = threading.Lock()
    return o

def makeGlobalThreadSafe(func):
    #Makes functions "subscribed" to it to be mutually exclusive choses, ergo
    #  only one of them can run at a time.
    #Used to give "Priority" to the important work but nothing in the implementation guarantees this.
    #  for now, this is done by making one monolithic "main" task that spans a lot of work,
    #  and then other small or "atomic" tasks that return control after a few instructions,
    #  meaning they return early and the priority func is not stalled that much
    def o(*args, **kwargs):
        makeGlobalThreadSafe.globalLock.acquire()
        try:
            value = func(*args, **kwargs)
        except:
            raise
        finally:
            makeGlobalThreadSafe.globalLock.release()

        return value

    return o
makeGlobalThreadSafe.globalLock = threading.RLock()
####

def loadDictionary(firstLetter, length, dictionaryFolder=None, filenameFormat=None):
    firstLetter = firstLetter.lower()
    key = firstLetter + str(length)
    cacheDictionary(firstLetter, length, dictionaryFolder, filenameFormat)

    return loadDictionary.dictionaries[key]
loadDictionary.dictionaries = {}

@makeGlobalThreadSafe
@makeThreadSafe
def cacheDictionary(firstLetter, length, dictionaryFolder=None, filenameFormat=None):
    firstLetter = firstLetter.lower()
    key = firstLetter + str(length)
    #The key check is put here for now so that it is easily in a threaded environment,
    #  the lock will take effect even before checking membership, i.e  to prevent a race condition
    if key in loadDictionary.dictionaries:
        return

    dictionaryFolder = dictionaryFolder or "./"
    filenameFormat = filenameFormat or "words_{letter}{length}"
    #print("DEBUG: Loading new dictionary for", key)
    dictionary = set()
    ####Resolution delegated to caller for now so things like warning the user can be done by the caller
    #file = None
    #try:
        #file = open(dictionaryFolder + filenameFormat.format(letter=firstLetter.lower(), length=length))
    #except IOError:
        #loadDictionary.dictionaries[key] = set()
        #return

    #for word in file:
    for word in open(dictionaryFolder + filenameFormat.format(letter=firstLetter, length=length)):
        dictionary.add(word[:-1])   #last char is not included as that is the newline(\n) delimiter
    loadDictionary.dictionaries[key] = dictionary

def preloadDictionaries(dictionaryFolder=None, filenameFormat=None):
    dictionaryFolder = dictionaryFolder or "./"
    if not os.access(dictionaryFolder, os.F_OK):    #test for the existence of the supplied dictionary folder
        print("\nDictionary folder supplied does not exist.")
        return
    #IMPROV: Replace this rather naive brute force opening of dictionaries with a more sensible (and statistically backed) heuristic
    for fileName in os.listdir(dictionaryFolder):
        #For now, these are hardcoded for the name format words_{letter}{length}
        if "words_" not in fileName:
            continue
        firstLetter = fileName[6].lower()
        length = fileName[7:]
        key = firstLetter + length    #since length here came from a string path, no need to call str on it
        #print("DEBUG: Loading new dictionary for", key)
        try:
            cacheDictionary(firstLetter, length, dictionaryFolder, filenameFormat)
        except FileNotFoundError:
            loadDictionary.dictionaries[key] = set()

    print("\nDone preloading dictionaries")

def removeLetters(letters, letters_to_remove):
    letters = letters.lower()
    if len(letters_to_remove) == 0:
        return letters

    invalid_requests = set()
    letterList = list(letters)
    for entry in letters_to_remove:
        entry = entry.lower()
        if entry not in letterList:
            invalid_requests.add(entry)
            continue

        letterList.remove(entry)

    for letter in invalid_requests:
        print(letters_to_remove.count(letter), "copies of", letter.upper(), "were requested.", letters.count(letter), "such request were honored, which is the number of copies in the letter pool")
        print()

    return listToStr(letterList)

def getNumPermutations(letters, r):
    num_perms = 1
    for i in range(len(letters), len(letters) - r , -1):
        num_perms *= i

    return num_perms

def getNumDictionary(letters, r, dictionaryLengths):
    num_dictionary = 0
    r = str(r)
    for letter in set(letters):
        index = letter.lower() + r
        if index in dictionaryLengths:
            num_dictionary += dictionaryLengths[index]

    return num_dictionary

@makeGlobalThreadSafe
def getWords(letters, r, given=None, dictionaryFolder=None, dictionaryLengths=None):
    given = given or {}

    orig_letters = letters.lower()
    letters = sorted(removeLetters(orig_letters, given.values()))

    if 0 in given:
        firstLetters = [given[0]]
    else:
        firstLetters = sorted(set(letters))

    perm_r = r - len(given)
    r = str(r)
    possibilities = []

    if dictionaryFolder is not None:   #SLower speed > lower var mem consumption?
        dictionary = []
        firstLetter = ""
        if (dictionaryLengths is not None) and (getNumPermutations(letters, perm_r) > getNumDictionary(firstLetters, r, dictionaryLengths)):
            if getNumDictionary(firstLetters, r, dictionaryLengths) == 0:
               return []
            print("DEBUG: Dictionary override trigerred (%s > %s)" % (getNumPermutations(letters, perm_r), getNumDictionary(firstLetters, r, dictionaryLengths)))
            orig_letters = Counter(orig_letters)
            for letter in firstLetters:
                key = letter + r
                try:
                    dictionary = loadDictionary(letter, r, dictionaryFolder)
                except FileNotFoundError:
                    print("Dictionary for", key, "was not found. Skipping")
                    loadDictionary.dictionaries[key] = set()
                    continue
                for word in dictionary:
                    count_word = Counter(word)
                    for letter in count_word:
                        if (letter not in orig_letters) or (orig_letters[letter] < count_word[letter]):
                            break
                    else:
                        for index in given:
                            if word[index] != given[index].lower():
                                break
                        else:
                            possibilities.append(word)

            return possibilities
    else:
        print("No valid dictionary folder was supplied. Without a dictionary to check against, all permutations are printed instead.")

    dictionaryErrors = []    #misleading var name

    for perm in permutations(letters, perm_r):
        perm = listToStr(perm)
        for index in given:
            perm = perm[:index] + given[index] + perm[index:]

        if dictionaryFolder is not None:
            firstLetterNew = perm[0]
            if firstLetterNew != firstLetter:
                firstLetter = firstLetterNew

                try:
                    dictionary = loadDictionary(firstLetter, r, dictionaryFolder)
                except FileNotFoundError:
                    error = firstLetterNew.upper() + r
                    if error not in dictionaryErrors:
                        dictionaryErrors.append(error)    #improve dis
                    dictionary = None
                    loadDictionary.dictionaries[firstLetter + r] = set()

            if dictionary:
                if perm not in dictionary:
                    continue

        if perm not in possibilities:
            possibilities.append(perm)

    if len(dictionaryErrors) != 0:
        print("The dictionaries for the following were not found:")
        for error in dictionaryErrors:    #improve dis
            print(error[1:], "letter words starting with", error[0])
        print("Without a valid dictionary to check against, ALL permutations were output instead. Consider checking the dictionary files or the supplied dictionary folder.")        #improve dis

    return possibilities

def checkLetters(letters):
    if not letters:     #if len(letters) == 0, but does not raise an error when default == None and is faster, but less explicit
        return ""

    output = ""
    for letter in letters:
        if letter.isalpha():
            output += letter
        elif letter.isspace():
            print(letter, "is a space. Skipping.")
        else:
            print(letter, "not a letter. Skipping.")

    return output

def normalizeR(r, maxR=None, minR=1):
    #change to if maxR is not none or r < 0?
    if maxR is not None:
        if r < 0:
            r += maxR

        if r > maxR:
            print(r, "is greater than the maximum r of", maxR, ".Clipping it to the maximum.")
            r = maxR

    if minR is not None:
        if r < minR:
            print(r, "is less than the minimum r of ", minR, ".Clipping it to the minimum.")
            r = minR

    return r

def expandLetterRange(letterRange, lettersSource=None):
    lettersSource = lettersSource or alphabet
    if letterRange.count("-") > 1:
        raise ValueError(letterRange, "has too much of the '-' range delimiter.")

    start, end = letterRange.split("-")
    if not (start.isalpha() and end.isalpha()):
        raise ValueError(letterRange + " is an invalid letter range. Start and end must be singular letters.")
    start, end = map(lettersSource.index, (start, end))
    if start > end:
        step = -1
    else:
        step = 1

    return lettersSource[start:end:step] + lettersSource[end]    #here, the letter on index end is manually appended instead of doing [start:end-1] because that returns an empty list when end = 0

def expandRRange(rRange, maxR=None):
    for i in range(1, len(rRange)-1):
        next = rRange[i+1]
        if (rRange[i] == "-" and rRange[i-1].isnumeric()) and (next.isnumeric() or next == "-"):
            delimiter = i
            break
    else:
        raise ValueError(rRange, "is an invalid r range. R ranges must be delimited with a -")

    start, end = rRange[:delimiter], rRange[delimiter+1:]
    try:
        start, end = map(int, (start, end))
    except ValueError:
        raise ValueError("The range", rRange, "is Invalid. Start and end must be integers")

    start, end = map(lambda x: normalizeR(x, maxR), (start, end))
    if start > end:
        offset = -1
    else:
        offset = 1

    return list(range(start, end + offset, offset))

#def expandGivenRange(startIndex, letters, maxIndex=None, reverse=False):
def expandGivenRange(givenRange, maxIndex=None):
    for reverse in (False, True):
        int_start, int_end = getIntPos(givenRange, reverse=reverse, signed=True)
        if (int_start != int_end):
            startIndex = int(givenRange[int_start:int_end])
            if not reverse:
                letters = givenRange[int_end:]
            else:
                letters = givenRange[:int_start]
            break
    else:
        raise ValueError("The given range is malformed. It must be in the form of a reference index and letters e.g. 1ABCD or WXYZ26.")
    
    out = {}
    letters_len = len(letters)
    if startIndex < 0:
        startIndex += maxIndex + 1      #normalizeR not used here because this needs to be a user-facing index (index 1 == first letter, so index -1 should be 13 for maxIndex 13 (13 - 1 = 12 + 1)
    if reverse:
        startIndex = (startIndex - letters_len) + 1
    if startIndex < 1:
        raise IndexError("The computed starting index of the range is less than 1. This might come about when the end index is less than the length of the letters in the goven range (e.g. enough2)")

    if maxIndex is not None:
        if (startIndex + letters_len) - 1 > maxIndex:
            raise IndexError("The given range supplied goes beyond the maximum index")

    for letter in letters:
        out[startIndex] = letter
        startIndex += 1

    return out

def getLetters(default=None):
    if (default is not None) and (default != ""):
        default = checkLetters(default)
        print("Default letters:", default.upper())

    message = "Enter letters available: "
    letters = ""
    while True:
        inpt = input(message)
        if not inpt:    #if len(input) == 0
            letters = default
            break

        for num, entry in enumerate(inpt.split()):            
            ####Special letter pool modifier interpretation (setup)
            if entry[0] in ("-", "+"):
                if num == 0 and len(letters) == 0:
                    print("A modifier has been invoked without a set of letters. The default letters will be used instead.")
                    letters = default

                prefix = entry[0]
                entry = entry[1:]

            else:    #The prefix character(s) have no special meaning
                prefix = ""
            
            ####Special input interpretation (setup)
            if entry[0].isnumeric():
                int_start, int_end = getIntPos(entry)
                num = int(entry[int_start:int_end])
                entry = entry[int_end:]
            else:
                num = 1

            if "-" in entry:
                try:
                    entry = expandLetterRange(entry)
                except ValueError:
                    pass

            entry = checkLetters(entry)
            ####Special parsing operations (execution)
            ## There are 2 layers of special parsing operations: The modifiers and the special input parsers
            ## The modifiers change the letter pool or the cumulative letter pool
            ## The special input parsers that take formated input and translates it into a normal entry of letters
            if num > 1:    #Maybe just directly put entry = checkLetters(entry) * num above, something dropped for clarity of intent.
                entry = entry * num
            if prefix == "-":
                print("Removing letters:", entry.upper())
                letters = removeLetters(letters, entry)
            elif prefix == "+":
                print("Adding letters:", entry.upper())
                letters += entry
            else:    #There is no prefix
                letters += entry

        if len(letters) == 0:
            if default is None:
                message = "Letter list must not be empty. letters: "
            elif not default:     #if len(default) == 0, but does not raise an error when default == None and is faster, but less explicit
                message = "Default is invalid. Enter a valid list of letters: "
        else:
            break

    return letters.lower()

def getR(letters):
    maxR = len(letters)
    print("Maximum R is", maxR)

    rOut = []
    while True:
        rList = input("Enter the value of r: ").split(" ")
        rList = [r for r in rList if r != " " and r != ""]
        if len(rList) == 0:
            return [maxR]

        for r in rList:
            if "-" in r:
                try:
                    rOut.extend(expandRRange(r, maxR))
                except ValueError as e:
                    print(e)
                    pass
                else:
                    continue

            try:
                r = int(r)
            except ValueError:
                print(r, "is not a number. Skipping")
                continue
            if r < 0:   #if r is negative, get maxR - |r| so when the letter pool has 9 letters and the word contains 8, you can simply do -1
                r = maxR + r

            if r > maxR:
                print(r, "is greater than the maximum r value. Skipping.")
                continue
            elif r < 1:    #maybe even r < 2 but eh
                print(r, "is less than 1 which would mean it will not contain letters. Skipping.")
                continue
            elif r in rOut:
                inpt = input(str(r) + " is already in the R List. Skip? (Y/n): ")
                if inpt.lower() != "n":
                    continue

            rOut.append(r)

        if len(rOut) > 0:
            break

    return rOut

def getGiven(letters, r):
    inpt = input("Enter list of given likeso: 1G 5A: ")
    count  = Counter(letters)
    if len(inpt) == 0:
        return {}

    inpt = inpt.split(" ")
    givenRaw = {}
    for entry in inpt:
        entry = entry.upper()     #maybe do not make given uppercase as dictionary is lowercase. .upper() used here just for the UI
        try:
            #givenRaw.update(expandGivenRange(entry, r))
            #This loop is a simplified version of the entry checking below to handle the case when an already specified given index is referenced in the range
            #Ideally, this should be streamlined to the preexisting loop below, but this has to do for now.
            for index, letter in expandGivenRange(entry, r).items():
                if index in givenRaw:
                    print("A letter for the index", index, "has already been given (in the form of", index, "or", -r + index - 1, ", or referenced somewhere in a range). Skipping")
                    continue

                givenRaw[index] = letter
        except IndexError as error:
            print(entry)
            print(error)
        except ValueError:      #These are errors when the range is invalid, which is triggered when expandGivenRange is used on normal given input
            pass
        else:
            continue

        index = entry[:-1]
        try:
            index = int(index)
        except:
            print(entry, "is malformed.", index , "should be an integer. Skipping")
            continue

        if index < 0:
            index = (r + index) + 1

        if index < 1:  #Check again if index is 0 or negative. This handles case when index is -15, and r is 14 (results to -1, and canonically indices shouldn't be negative
            print("The given index", index - r - 1, "is an invalid backward offset for a word with length", r, ". Skipping")    ####TODO: Make message more terse
            continue;

        letter = entry[-1]
        #move to checkGiven function?
        if not letter.isalpha():
            print(entry, "is malformed.", letter, "must be a letter in the alphabet. Skipping.")
            continue

        if index in givenRaw:
            print("A letter for the index", index, "has already been given (in the form of", index, "or", -r + index - 1, ", or referenced somewhere in a range). Skipping")
            continue

        givenRaw[index] = letter

    #Separated in here are the context aware checks in order to not complicate the loop above which is dedicated for encoding input to internal representation
    given = {}
    letters = letters.upper()       #.upper() used here so that the UI is consistent with showing uppercase letters
    for index in givenRaw:
        letter = givenRaw[index].lower()
        if index > r:
            print(entry, ":", index, "exceeds r length. Skipping.")
            continue
        elif letter not in count:
            print(entry, ":", letter.upper(), "not in", letters, "Skipping.")
            continue
        elif count[letter] < 1:
            print("Too much", letter.upper(), "given request. There are only",
                  letters.count(letter), letter.upper(), "'s in", letters)
            continue

        given[index - 1] = letter   #Should output be 0-indexed or should the conversion be delegated to users of the output?
        count[letter] -= 1

    return given

def printGiven(given, r):
    output = ["_" for i in range(r)]

    for index in given:
        output[index] = given[index].upper()

    print(listToStr(output, end=" "))

def printPerms(perms):
    if len(perms) == 0:
        print("The given letters do not match with any word in the dictionary.")
        return

    for index, perm in enumerate(perms, start=1):
        print(index, end=". "); print(perm.upper())    #check if str() + . is faster than using end

def wordscapey(letters, rList, dictionaryFolder=None, dictionaryLengths=None):
    for r in rList:
        print("R:", r)
        given = getGiven(letters, r)
        printGiven(given, r)
        possibilities = getWords(letters, r, given, dictionaryFolder, dictionaryLengths)
        printPerms(possibilities)
        print()

############################################################
#Globals
appFolder = "/mnt/sdcard/qpython/projects3/Wordscapey/"
dictionaryFolder = appFolder + "dictionaries/"
defaultLettersFile = appFolder + "defaultLetters"
dictionaryLengthsFile = appFolder + "dictionaryLengths"
############################################################

if __name__ == "__main__":
    from datetime import datetime   #for measuring speed
    try:
        letters = open(defaultLettersFile, "r").readline()
    except FileNotFoundError:
        print("The given default letters file (%s) does not exist." % defaultLettersFile)
        letters = None

    try:
        dictionaryLengths = json.load(open(dictionaryLengthsFile))
    except (FileNotFoundError, ValueError):
        print("Invalid dictionary lengths file supplied.")
        dictionaryLengths = None

    if not os.access(dictionaryFolder, os.F_OK | os.R_OK | os.X_OK):
        print("Dictionary folder supplied is invalid. Check if it exists and that you have the appropriate permissions to access it.")
        dictionaryFolder = None

    dictionaryCacherThread = threading.Thread(target=preloadDictionaries, args=(dictionaryFolder, ))
    dictionaryCacherThread.start()
    exit = "n"
    while exit != "y":
        letters = getLetters(default=letters)
        rList = getR(letters)

        print("Letters:", letters.upper())
        print("R's:", rList)

        before = datetime.now()
        wordscapey(letters, rList, dictionaryFolder, dictionaryLengths)
        print("Time:", str(datetime.now() - before))
        exit = input("Exit? (y/N) ").lower()
        print("\n\n")

    file = open(defaultLettersFile, "w")
    file.write(letters.upper())
    file.close()
